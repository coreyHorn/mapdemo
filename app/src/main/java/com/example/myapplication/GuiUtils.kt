package com.example.myapplication

import android.content.Context

object GuiUtils {

    fun dpToPx(ctx: Context, dp: Int): Float {
        val density = ctx.getResources().getDisplayMetrics().density
        return dp * density
    }

    fun pxToDp(ctx: Context, px: Int): Float {
        val density = ctx.getResources().getDisplayMetrics().density
        return px / density
    }
}