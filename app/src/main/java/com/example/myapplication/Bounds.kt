package com.example.myapplication

import com.google.android.gms.maps.model.LatLng


class Bounds {
    var northeast: Northeast? = null
        internal set
    var southwest: Southwest? = null
        internal set

    val southwestLatLng: LatLng
        get() =
            LatLng(
                java.lang.Double.parseDouble(southwest!!.lat!!),
                java.lang.Double.parseDouble(southwest!!.lng!!)
            )

    val northeastLatLng: LatLng
        get() =
            LatLng(
                java.lang.Double.parseDouble(northeast!!.lat!!),
                java.lang.Double.parseDouble(northeast!!.lng!!)
            )

}