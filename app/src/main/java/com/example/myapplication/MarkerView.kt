package com.example.myapplication

import android.content.Context
import android.graphics.Point
import android.view.View
import android.widget.FrameLayout
import com.google.android.gms.maps.model.LatLng


abstract class MarkerView private constructor(context: Context) : FrameLayout(context) {

    abstract var point: Point
    abstract var latLng: LatLng

    constructor(context: Context, latLng: LatLng, point: Point) : this(context) {
        this.latLng = latLng
        this.point = point
    }

    fun lat(): Double {
        return latLng.latitude
    }

    fun lng(): Double {
        return latLng.longitude
    }

    fun point(): Point {
        return point
    }

    fun latLng(): LatLng {
        return latLng
    }

    abstract fun show()

    abstract fun hide()

    abstract fun refresh(point: Point)
}