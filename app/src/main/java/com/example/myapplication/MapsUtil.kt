package com.example.myapplication

import android.util.DisplayMetrics
import android.view.WindowManager


object MapsUtil {
    val MAP_BITMAP_KEY = "map_bitmap_key"
    val DEFAULT_ZOOM = 150
    private val LATITUDE_INCREASE_FACTOR = 1.5
    var DEFAULT_MAP_PADDING = 30

    fun increaseLatitude(bounds: Bounds): String {
        val southwestLat = bounds.southwest!!.latD
        val northeastLat = bounds.northeast!!.latD
        val updatedLat = LATITUDE_INCREASE_FACTOR * Math.abs(northeastLat!! - southwestLat!!)
        return (southwestLat - updatedLat).toString()
    }

    fun calculateWidth(windowManager: WindowManager): Int {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        return metrics.widthPixels
    }

    fun calculateHeight(windowManager: WindowManager, paddingBottom: Int): Int {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        return metrics.heightPixels - paddingBottom
    }
}