package com.example.myapplication

import android.content.Context
import android.graphics.Color
import android.graphics.Point
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.CameraUpdateFactory
import android.util.AttributeSet
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Polyline
import android.widget.FrameLayout


class MapOverlayLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    var markersList: MutableList<MarkerView> = ArrayList()
    var currentPolyline: Polyline? = null
    lateinit var googleMap: GoogleMap
    lateinit var polylines: ArrayList<LatLng>

    val currentLatLng: LatLng
        get() = LatLng(
            googleMap.cameraPosition.target.latitude,
            googleMap.cameraPosition.target.longitude
        )

    init {
        markersList = ArrayList()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    fun addMarker(view: MarkerView) {
        markersList.add(view)
        addView(view)
    }

    fun removeMarker(view: MarkerView) {
        markersList.remove(view)
        removeView(view)
    }

    fun showAllMarkers() {
        for (i in markersList.indices) {
            markersList[i].show()
        }
    }

    fun hideAllMarkers() {
        for (i in markersList.indices) {
            markersList[i].hide()
        }
    }

    fun showMarker(position: Int) {
        markersList[position].show()
    }

    private fun refresh(position: Int, point: Point) {
        markersList[position].refresh(point)
    }

    fun setupMap(googleMap: GoogleMap) {
        this.googleMap = googleMap
    }

    fun refresh() {
        val projection = googleMap.projection
        for (i in markersList.indices) {
            refresh(i, projection.toScreenLocation(markersList[i].latLng()))
        }
    }

    fun setOnCameraIdleListener(listener: GoogleMap.OnCameraIdleListener) {
        googleMap.setOnCameraIdleListener(listener)
    }

    fun setOnCameraMoveListener(listener: GoogleMap.OnCameraMoveListener) {
        googleMap.setOnCameraMoveListener(listener)
    }

    fun moveCamera(latLngBounds: LatLngBounds) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 150))
    }

    fun animateCamera(bounds: LatLngBounds) {
        val width = width
        val height = height
        val padding = MapsUtil.DEFAULT_MAP_PADDING
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding))
    }

    fun addPolyline(polylines: ArrayList<LatLng>) {
        this.polylines = polylines
        val options = PolylineOptions()
        for (i in 1 until polylines.size) {
            options.add(polylines[i - 1], polylines[i]).width(10f).color(Color.RED).geodesic(true)
        }
        currentPolyline = googleMap.addPolyline(options)
    }

    fun removeCurrentPolyline() {
        if (currentPolyline != null) currentPolyline!!.remove()
    }
}