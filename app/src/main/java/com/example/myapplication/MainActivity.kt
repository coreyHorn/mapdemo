package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        map.onCreate(savedInstanceState)

        map.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onStop() {
        map.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        map.onDestroy()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        map.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onMapReady(map: GoogleMap?) {
        Log.d("stuff", "onMapReady")
        mapOverlay.setupMap(map!!)

        val virginiaTest = LatLng(34.420505, -82.661763)

        map.setOnCameraIdleListener {
            val point = map.projection.toScreenLocation(virginiaTest)
            val marker = FlameMarkerView(baseContext, virginiaTest, point)

            mapOverlay.addMarker(marker)

            mapOverlay.refresh()

            marker.show()

            map.setOnCameraIdleListener {  }
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(virginiaTest, 18f))
        mapOverlay.setOnCameraMoveListener(GoogleMap.OnCameraMoveListener { mapOverlay.refresh() })
    }
}
