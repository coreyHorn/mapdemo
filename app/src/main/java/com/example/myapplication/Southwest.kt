package com.example.myapplication

class Southwest {

    var lat: String? = null

    var lng: String? = null
        internal set

    val latD: Double?
        get() = java.lang.Double.parseDouble(lat!!)
}