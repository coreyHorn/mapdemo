package com.example.myapplication

class Northeast {

    var lat: String? = null
        internal set
    var lng: String? = null
        internal set

    val latD: Double?
        get() = java.lang.Double.parseDouble(lat!!)
}