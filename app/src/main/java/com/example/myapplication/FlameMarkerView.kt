package com.example.myapplication

import android.animation.AnimatorSet
import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import android.widget.FrameLayout



class FlameMarkerView(context: Context, override var latLng: LatLng, override var point: Point): MarkerView(context, latLng, point) {

    private lateinit var showAnimatorSet: AnimatorSet
    private lateinit var hideAnimatorSet: AnimatorSet

    init {
        inflate(context, R.layout.marker_flame, this)
    }

    override fun show() {
//        showAnimatorSet.start()
    }

    override fun hide() {
//        hideAnimatorSet.start()
    }

    override fun refresh(point: Point) {
        updateLayoutParams(point)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

    }

    private fun updateLayoutParams(point: Point) {
        Log.d("stuff", "updateLayoutParams")
        this.point = point
        val params = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.WRAP_CONTENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        params.width = GuiUtils.dpToPx(context, 60).toInt()
        params.height = GuiUtils.dpToPx(context, 60).toInt()
        params.leftMargin = point.x - params.width / 2
        params.topMargin = point.y - params.height / 2
        super.setLayoutParams(params)
        invalidate()
    }
}